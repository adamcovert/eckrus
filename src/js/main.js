$(document).ready(function () {

  $('.burger').on('click', function () {
    $('.page-header__nav').toggleClass('page-header__nav--is-open');
    $(this).toggleClass('burger--is-active');
  });

  var swiper = new Swiper('.promo__slider', {
    navigation: {
      prevEl: '.promo__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.promo__slider-navigation .slider-navigation__btn--next'
    },
  });

  var swiper = new Swiper('.partners__slider', {
    loop: true,
    spaceBetween: 20,
    navigation: {
      prevEl: '.partners__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.partners__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 2
      },
      576: {
        slidesPerView: 4
      },
      768: {
        slidesPerView: 5
      },
      992: {
        slidesPerView: 6
      }
    }
  });

  var swiper = new Swiper('.featured-works__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.featured-works__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.featured-works__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.featured-works__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      576: {
        slidesPerView: 2
      }
    }
  });

  var swiper = new Swiper('.work-card__slider', {
    navigation: {
      prevEl: '.work-card__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.work-card__slider-navigation .slider-navigation__btn--next'
    },
  });

  var swiper = new Swiper('.equipment-card__slider', {
    navigation: {
      prevEl: '.equipment-card__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.equipment-card__slider-navigation .slider-navigation__btn--next'
    },
  });
});